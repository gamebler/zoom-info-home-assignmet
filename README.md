# AngularZoominfoTask
ZoomInfo Home Assignmet
# Part 1
## Install dependencies 
run `npm i`
## Run Project 
run `ng serve`

# Part 2
Angular directive does not support static HTML because it's an angular feature.
The way I chose to implement it, is by exporting it as angular web component.
## Build web app components
run `npm run build:component`

## Run web app components
run `npm run run:component` and navigate to http://localhost:5000
