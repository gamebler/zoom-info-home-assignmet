import { Component, OnInit } from '@angular/core';
import { fadeInOutAnimation } from '../animations/fade-in-out-animation';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css'],
  animations: [fadeInOutAnimation(200)]
})
export class PopupComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
