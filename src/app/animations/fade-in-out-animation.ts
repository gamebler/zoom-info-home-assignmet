import {
    animate,
    AnimationTriggerMetadata,
    state,
    style,
    transition,
    trigger
  } from "@angular/animations";
  
  export const fadeInOutAnimation: (delay: number) => AnimationTriggerMetadata = (
    delay: number
  ) =>
    trigger("fadeInOut", [
      state(
        "void",
        style({
          opacity: 0
        })
      ),
      transition("void <=> *", animate(delay))
    ]);
  