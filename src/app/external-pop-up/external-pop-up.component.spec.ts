import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalPopUpComponent } from './external-pop-up.component';

describe('ExternalPopUpComponent', () => {
  let component: ExternalPopUpComponent;
  let fixture: ComponentFixture<ExternalPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExternalPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
