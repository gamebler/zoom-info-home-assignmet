import { BrowserModule } from '@angular/platform-browser';
import { Injector, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PopupComponent } from './popup/popup.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppPopUpDirective } from './popup.directive';
import { createCustomElement } from '@angular/elements';
import { ExternalPopUpComponent } from './external-pop-up/external-pop-up.component';
@NgModule({
  declarations: [
    AppComponent,
    PopupComponent,
    AppPopUpDirective,
    ExternalPopUpComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    const el = createCustomElement(ExternalPopUpComponent, { injector });
    customElements.define('pop-up', el);
  }
  ngDoBootstrap() { }
}
