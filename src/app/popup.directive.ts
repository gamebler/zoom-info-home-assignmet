import {
    Directive,
    Input,
    HostListener,
    TemplateRef,
    ViewContainerRef,
    ComponentRef,
    ComponentFactoryResolver
  } from "@angular/core";
import { PopupComponent } from './popup/popup.component';
  
  @Directive({
    selector: "[appPopup]"
  })
  export class AppPopUpDirective {
    @Input("appPopup") appPopup: TemplateRef<any>;
    private componentRef: ComponentRef<PopupComponent>;
    constructor(
      private resolver: ComponentFactoryResolver,
      private vcr: ViewContainerRef
    ) {}
  
    @HostListener("mouseenter") onMouseEnter() {
      this.create();
    }
    @HostListener("mouseleave") onMouseLeave() {
      this.destroy();
    }
  
    create() {
      const factory = this.resolver.resolveComponentFactory(PopupComponent);
      this.componentRef = this.vcr.createComponent(
        factory,
        0,
        null,
        this.generateNgContent()
      );
    }
  
    generateNgContent() {
      const context = {};
      const viewRef = this.appPopup.createEmbeddedView(context);
      return [viewRef.rootNodes];
    }
  
    destroy() {
      this.componentRef.destroy();
      this.componentRef = null;
    }
  }
  