const fs = require('fs-extra');
const concat = require('concat');

build = async () =>{
    const files = [
        './dist/angular-zoominfo-task/runtime.js',
        './dist/angular-zoominfo-task/polyfills.js',
        './dist/angular-zoominfo-task/main.js'
      ];
    
      await fs.ensureDir('webcomponent');
      await concat(files, 'webcomponent/popup.js');
}
build();